#ifndef TEST_TEST_H
#define TEST_TEST_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define TEST_EQUAL_STRINGS(exp1,exp2,msg) \
	{ \
		char *s1 = exp1, *s2 = exp2; \
		if (strcmp(s1, s2)) { \
			fprintf(stderr, "ERROR: \"%s\" != \"%s\"\n", s1, s2); \
			exit(1); \
		} \
	}

#define TEST_EQUAL_STRINGS_AND_FREE_FIRST(exp1,exp2,msg) \
	{ \
		char *s1 = exp1, *s2 = exp2; \
		if (strcmp(s1, s2)) { \
			fprintf(stderr, "ERROR at %s: \"%s\" != \"%s\"\n", msg, s1, s2); \
			exit(1); \
		} \
		free(s1); \
	}

#define TEST_EQUAL(exp1,exp2,msg) \
	{ \
		int x = exp1, y = exp2; \
		if (x != y) { \
			fprintf(stderr, "ERROR in %s: %d != %d\n", msg, (int)x, (int)y); \
			exit(1); \
		} \
	}

#endif // TEST_TEST_H

