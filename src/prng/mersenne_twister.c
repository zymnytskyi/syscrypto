#include <stdint.h>

#include <syscrypto/prng.h>
#include <util/memory.h>

#define PRNG_STATE_SIZE 624

typedef struct {
	prng_t super;
	uint32_t index;
	uint32_t mt[PRNG_STATE_SIZE];
} prng_mt_t;

prng_t*
prng_mt_new(int seed)
{
	int i;
	prng_mt_t* this = ALLOC_STRUCT(prng_mt_t);

	this->super.type = PRNG_GENERATOR_MT;
	this->index = PRNG_STATE_SIZE;
	this->mt[0] = seed;

	for (i = 1; i < PRNG_STATE_SIZE; i++) {
		this->mt[i] = 1812433253 * (this->mt[i - 1] ^ this->mt[i - 1] >> 30) + i;
	}

	return (prng_t*)this;
}

uint32_t
prng_mt_rand(prng_t* prng)
{
	int i;
	uint32_t y;
	prng_mt_t* this = (prng_mt_t*)prng;

	if (this->index >= PRNG_STATE_SIZE) {
		for (i = 0; i < PRNG_STATE_SIZE; i++) {
			y = (this->mt[i] & 0x80000000) + (this->mt[(i + 1) % PRNG_STATE_SIZE] & 0x7FFFFFFF);
			this->mt[i] = this->mt[(i + 397) % PRNG_STATE_SIZE] ^ y >> 1;

			if (y % 2 != 0) {
				this->mt[i] = this->mt[i] ^ 0x9908B0DF;
			}
		}

		this->index = 0;
	}

	y = this->mt[this->index];

	y ^= y >> 11;
	y ^= y << 7 & 2636928640;
	y ^= y << 15 & 4022730752;
	y ^= y >> 18;

	this->index++;

	return y;
}

void
prng_mt_delete(prng_t* this) {
	free(this);
}

