#include <syscrypto/bignum.h>
#include <syscrypto/prng.h>

#include <util/memory.h>

typedef struct {
	bignum_t* n;
	bignum_t* e;
	bignum_t* d;
	bignum_t* p;
	bignum_t* q;
} rsa_key_t;

static bignum_t*
rsa_random_prime(int length)
{
	uint8_t* bytes = ALLOC_STRUCTS(uint8_t, length);
	prng_t* prng = prng_new_seeded(PRNG_GENERATOR_DEFAULT);
	bignum_t *number, *t, *one = bignum_from_uint(1);

	prng_random_bytes(prng, bytes, length);
	number = bignum_from_bytes(bytes, length);

	while (!bignum_is_prime(number)) {
		t = bignum_add(number, one);
		bignum_delete(number);
		number = t;
	}

	free(bytes);
	prng_delete(prng);

	return number;
}

static bignum_t*
rsa_get_coprime(bignum_t* n) {
	prng_t* prng = prng_new_seeded(PRNG_GENERATOR_DEFAULT);
	bignum_t *result = NULL, *gcd, *one = bignum_from_uint(1);
	int keep = 1;

	while (keep) {
		if (result) bignum_delete(result);

		result = bignum_from_uint(prng_rand(prng));
		gcd = bignum_gcd(n, result);

		if (bignum_cmp(gcd, one) == 0) {
			keep = 0;
		}

		bignum_delete(gcd);
	}

	bignum_delete(one);
	return result;
}

rsa_key_t*
rsa_generate_key(int bits)
{
	rsa_key_t* result;
	int bytes = bits / 8;
	bignum_t *q, *p, *n, *t;
	bignum_t *qminus1, *pminus1, *fi;
	bignum_t *one = bignum_from_uint(1);
	bignum_t *e, *d;

	q = rsa_random_prime(bytes / 2);
	p = rsa_random_prime(bytes / 2);

	n = bignum_mul(q, p);

	qminus1 = bignum_sub(q, one);
	pminus1 = bignum_sub(p, one);
	fi = bignum_mul(qminus1, pminus1);
	e = rsa_get_coprime(fi);
	bignum_euclidean_extended(NULL, NULL, &d, fi, e);

	if (d->minus == 1) {
		t = bignum_add(d, fi);
		bignum_delete(d);
		d = t;
	}

	result = ALLOC_STRUCT(rsa_key_t);
	result->p = p;
	result->q = q;
	result->n = n;
	result->e = e;
	result->d = d;

	return result;
}

void
rsa_encrypt(rsa_key_t* key, uint8_t* in, uint32_t len, uint8_t** out, uint32_t* outlen)
{
}

void
rsa_encrypt(rsa_key_t* key, uint8_t* in, uint32_t len, uint8_t** out, uint32_t* outlen)
{
}

int main(void) {
	rsa_key_t* key = rsa_generate_key(2048);
	printf("%s\n", bignum_to_str(key->p, 10));
	printf("%s\n", bignum_to_str(key->q, 10));
	printf("%s\n", bignum_to_str(key->n, 10));
	printf("%s\n", bignum_to_str(key->e, 10));
	printf("%s\n", bignum_to_str(key->d, 10));
	return 0;
}

