#include <syscrypto/bignum.h>

#include <util/math.h>
#include <util/memory.h>

#include <string.h>
#include <ctype.h>

#define BIGNUM_DIGIT_BITS 32
#define BIGNUM_BASE (((uint64_t)1)<<BIGNUM_DIGIT_BITS)
#define BIGNUM_MASK (BIGNUM_BASE-1)

#define ALLOC_BIGNUM(b,l) \
	b = ALLOC_STRUCT(bignum_t); \
	(b)->minus = 0; \
	(b)->len = l; \
	(b)->places = ALLOC_STRUCTS(uint32_t, (b)->len);

enum errcodes {
	ERR_BASE_TOO_BIG = -1,
};

static char digit_chars[] = "0123456789abcdefghijklmnopqrstuvwxyz";

bignum_t* bignum_add_unsigned(bignum_t* left, bignum_t* right);

bignum_t*
bignum_from_uint(unsigned int i)
{
	bignum_t* bn = ALLOC_STRUCT(bignum_t);

	bn->len = 1;
	bn->minus = 0;
	bn->places = ALLOC_STRUCTS(uint32_t, bn->len);
	bn->places[0] = i;

	return bn;
}

bignum_t*
bignum_new(void)
{
	return bignum_from_uint(0);
}

bignum_t*
bignum_from_str(const char* str, int base)
{
	if (base > sizeof(digit_chars) - 1) {
		return NULL;
	}

	bignum_t* bn = bignum_from_uint(0);
	bignum_t *b = bignum_from_uint(base);
	bignum_t *v = bignum_from_uint(1);
	bignum_t *n, *t1, *t2;

	int len = strlen(str);
	int end = 0;

	if (str[0] == '-') {
		end++;
	}

	for (int i = len - 1; i >= end; i--) {
		char c = tolower(str[i]);
		n = NULL;

		for (int j = 0; j < base; j++) {
			if (digit_chars[j] == c) {
				n = bignum_from_uint(j);
				break;
			}
		}

		if (!n) continue;

		t1 = bignum_mul(n, v);
		bignum_delete(n);

		t2 = bignum_add_unsigned(t1, bn);
		bignum_delete(t1);

		bignum_delete(bn);
		bn = t2;

		t1 = bignum_mul(v, b);
		bignum_delete(v);
		v = t1;
	}

	if (str[0] == '-') {
		bn->minus = 1;
	}

	bignum_delete(v);
	bignum_delete(b);
	return bn;
}

bignum_t*
bignum_from_bytes(uint8_t* bytes, uint32_t n)
{
	uint32_t full, len, k;
	int i;
	uint32_t* data;
	bignum_t* result;

	full = len = n / 4;
	if (n % 4 != 0) {
		len++;
	}

	ALLOC_BIGNUM(result, len);
	data = (uint32_t*)bytes;

	for (i = 0; i < full; i++) {
		result->places[i] = data[i];
	}

	if (full != len) {
		result->places[len - 1] = 0;
		k = 1;

		for (i = 0; i < n % 4; i++) {
			result->places[len - 1] += k * bytes[full * 4 + i];
			k *= 256;
		}
	}

	return result;
}

bignum_t*
bignum_copy(bignum_t* b)
{
	bignum_t* result;
	ALLOC_BIGNUM(result, b->len);
	result->minus = b->minus;

	for (int i = 0; i < result->len; i++) {
		result->places[i] = b->places[i];
	}

	return result;
}

void
bignum_delete(bignum_t* b)
{
	free(b->places);
	free(b);
}

char*
bignum_to_str(bignum_t* bn, int base)
{
	bignum_t* zero = bignum_from_uint(0);
	bignum_t* b = bignum_from_uint(base);
	bignum_t *q, *r;
	int written = 0, offset = 0;
	int size = 10;
	char *tmp = ALLOC_STRUCTS(char, size);
	char* result;
	int minus = bn->minus;
	int isZero = 1;

	bn = bignum_copy(bn);

	for (int i = 0; i < bn->len; i++) {
		if (bn->places[i] != 0) {
			isZero = 0;
			break;
		}
	}

	if (isZero) {
		tmp[written++] = '0';
	} else {
		bn->minus = 0;
	}

	while (bignum_cmp(bn, zero) > 0) {
		bignum_div(&q, &r, bn, b);
		bignum_delete(bn);
		bn = q;

		if (written + 1 >= size) {
			size *= 2;
			REALLOC_STRUCTS(char, size, tmp);
		}

		tmp[written++] = digit_chars[r->places[0]];

		bignum_delete(r);
	}

	bignum_delete(zero);
	bignum_delete(b);
	bignum_delete(bn);

	if (minus) {
		offset = 1;
	}

	result = ALLOC_STRUCTS(char, written + offset + 1);
	
	for (int i = 0; i < written; i++) {
		result[i + offset] = tmp[written - i - 1];
	}

	if (offset) {
		result[0] = '-';
	}

	result[written + offset] = 0;
	free(tmp);
	return result;
}

bignum_t*
bignum_add_unsigned(bignum_t* left, bignum_t* right)
{
	int i, k = 0;
	bignum_t* result = ALLOC_STRUCT(bignum_t);
	result->len = MAX(left->len, right->len);
	result->places = ALLOC_STRUCTS(uint32_t, result->len);

	for (i = 0; i < result->len; i++) {
		uint64_t n = (uint64_t)(i < left->len ? left->places[i] : 0) + (uint64_t)(i < right->len ? right->places[i] : 0) + k;
		result->places[i] = (uint32_t)n;

		if (n >= BIGNUM_BASE) {
			k = 1;
		} else {
			k = 0;
		}
	}

	if (k == 1) {
		result->len++;
		REALLOC_STRUCTS(uint32_t, result->len, result->places);
		result->places[result->len - 1] = 1;
	}

	return result;
}

bignum_t*
bignum_sub_unsigned(bignum_t* left, bignum_t* right)
{
	int i, k = 0;
	bignum_t* result;
	ALLOC_BIGNUM(result, MAX(left->len, right->len));

	for (i = 0; i < result->len; i++) {
		int64_t n = (int64_t)(i < left->len ? left->places[i] : 0) - (int64_t)(i < right->len ? right->places[i] : 0) + k;
		result->places[i] = (uint32_t)n;

		if (n < 0) {
			k = -1;
		} else {
			k = 0;
		}
	}

	if (k == -1) {
		for (i = 0; i < result->len; i++) {
			result->places[i] = ~result->places[i];
		}

		result->minus = 1;
		result->places[0] += 1;
	}

	return result;
}

int
bignum_cmp(bignum_t* a, bignum_t* b) {
	bignum_t* s = bignum_sub_unsigned(a, b);

	for (int i = 0; i < s->len; i++) {
		if (s->places[i] != 0) {
			bignum_delete(s);
			return s->minus ? -1 : 1;
		}
	}

	bignum_delete(s);
	return 0;
}

bignum_t*
bignum_add(bignum_t* left, bignum_t* right)
{
	bignum_t* result;
	int lsign = left->minus, rsign = right->minus;
	int cmp;

	left->minus = 0;
	right->minus = 0;

	if (lsign == rsign) {
		result = bignum_add_unsigned(left, right);
		result->minus = left->minus = right->minus = lsign;
		return result;
	}
	
	result = bignum_sub_unsigned(left, right);

	if (lsign) {
		result->minus = !result->minus;
	}

	left->minus = lsign;
	right->minus = rsign;

	return result;
}

bignum_t*
bignum_sub(bignum_t* left, bignum_t* right)
{
	bignum_t* result;
	int lsign = left->minus, rsign = right->minus;
	int cmp;

	left->minus = 0;
	right->minus = 0;

	if (lsign != rsign) {
		result = bignum_add_unsigned(left, right);
		result->minus = left->minus = right->minus = lsign;
		return result;
	}
	
	result = bignum_sub_unsigned(left, right);

	if (lsign) {
		result->minus = !result->minus;
	}

	return result;
}

bignum_t*
bignum_mul_unsigned(bignum_t* left, bignum_t* right)
{
	int i, j;
	bignum_t* result = ALLOC_STRUCT(bignum_t);
	result->len = left->len + right->len;
	result->places = ALLOC_STRUCTS(uint32_t, result->len);
	memset(result->places, 0, sizeof(uint32_t) * result->len);

	for (j = 0; j < right->len; j++) {
		uint64_t k = 0;
		for (i = 0; i < left->len; i++) {
			uint64_t t = (uint64_t)left->places[i] * (uint64_t)right->places[j] + (uint64_t)result->places[i + j] + k;
			result->places[i + j] = (uint32_t)t;
			k = t / BIGNUM_BASE;
		}
		result->places[j + left->len] = k;
	}

	return result;
}

bignum_t*
bignum_mul(bignum_t* left, bignum_t* right)
{
	bignum_t* result = bignum_mul_unsigned(left, right);
	if (left->minus == right->minus) {
		result->minus = left->minus;
	} else {
		result->minus = 1;
	}
	return result;
}

static uint32_t nlz(uint64_t x) {
   uint32_t n = 0;
   if (x == 0) return 32;
   if (x <= 0x0000FFFF) { n += 16; x <<= 16; }
   if (x <= 0x00FFFFFF) { n += 8; x <<= 8; }
   if (x <= 0x0FFFFFFF) { n += 4; x <<= 4; }
   if (x <= 0x3FFFFFFF) { n += 2; x <<= 2; }
   if (x <= 0x7FFFFFFF) { n += 1; }

   return n;
}

int
bignum_div(bignum_t** pq, bignum_t** pr, bignum_t* left, bignum_t* right)
{
	uint32_t m = left->len;
	uint32_t n = right->len;

	bignum_t* q;
	bignum_t* r;
	bignum_t* u;
	bignum_t* v;

	uint64_t p, s, qhat, rhat;
	int64_t k, t;
	int i, j;

	while (m > 1 && left->places[m - 1] == 0) m--; // strip lead zeros
	while (n > 1 && right->places[n - 1] == 0) n--; // strip lead zeros

	if (right->places[n - 1] == 0) { // division by zero
		return 1;
	}

	if (m < n) {
		if (pq) {
			*pq = bignum_from_uint(0);
		}

		if (pr) {
			*pr = bignum_copy(left);
		}

		return 0;
	}

	if (n == 1) {
		k = 0;

		ALLOC_BIGNUM(q, m);
		ALLOC_BIGNUM(r, 1);

		for (j = m - 1; j >= 0; j--) {
			q->places[j] = (k * BIGNUM_BASE + left->places[j]) / right->places[0];
			k = (k * BIGNUM_BASE + left->places[j]) - q->places[j] * right->places[0];
		}

		r->places[0] = k;

		if (pq) *pq = q; else bignum_delete(q);
		if (pr) *pr = r; else bignum_delete(r);

		return 0;
	}

	ALLOC_BIGNUM(q, m - n + 1);
	ALLOC_BIGNUM(r, n);

	s = nlz(right->places[n - 1]);

	ALLOC_BIGNUM(v, n);

	for (i = n - 1; i > 0; i--) {
		v->places[i] = (right->places[i] << s) | ((uint64_t)right->places[i - 1] >> (BIGNUM_DIGIT_BITS - s));
	}

	v->places[0] = right->places[0] << s;

	ALLOC_BIGNUM(u, m + 1);

	u->places[m] = (uint64_t)left->places[m - 1] >> (BIGNUM_DIGIT_BITS - s);

	for (i = m - 1; i > 0; i--) {
		u->places[i] = (left->places[i] << s) | ((uint64_t)left->places[i - 1] >> (BIGNUM_DIGIT_BITS - s));
	}

	u->places[0] = left->places[0] << s;

	for (j = m - n; j >= 0; j--) {
		qhat = ((uint64_t)u->places[j + n] * BIGNUM_BASE + u->places[j + n - 1]) / v->places[n - 1];
		rhat = ((uint64_t)u->places[j + n] * BIGNUM_BASE + u->places[j + n - 1]) - qhat * v->places[n - 1];

		while (1) {
			if (qhat >= BIGNUM_BASE || qhat * v->places[n - 2] > BIGNUM_BASE * rhat + u->places[j + n - 2]) {
				qhat -= 1;
				rhat += v->places[n - 1];

				if (rhat < BIGNUM_BASE) continue;
			}

			break;
		}

		k = 0;

		for (i = 0; i < n; i++) {
			p = qhat * v->places[i];
			t = u->places[i + j] - k - (p & BIGNUM_MASK);
			u->places[i + j] = t;
			k = (p >> BIGNUM_DIGIT_BITS) - (t >> BIGNUM_DIGIT_BITS);
			//printf("%ld %lu\n", t, k);
		}

		t = u->places[j + n] - k;
		u->places[j + n] = t;

		q->places[j] = qhat;

		if (t < 0) {
			//printf("t < 0\n");
			q->places[j] -= 1;
			k = 0;
			for (i = 0; i < n; i++) {
				t = u->places[i + j] + v->places[i] + k;
				u->places[i + j] = t;
				k = t / BIGNUM_BASE;
			}

			u->places[j + n] += k;
		}
	}

	for (i = 0; i < n; i++) {
		r->places[i] = (u->places[i] >> s) | ((uint64_t)u->places[i + 1] << (BIGNUM_DIGIT_BITS - s));
	}

	r->places[n - 1] = u->places[n - 1] >> s;

	if (pq) *pq = q; else bignum_delete(q);
	if (pr) *pr = r; else bignum_delete(r);

	bignum_delete(u);
	bignum_delete(v);

	return 0;
}

bignum_t*
bignum_mod_exp(bignum_t* b, bignum_t* e, bignum_t* m)
{
	bignum_t* zero = bignum_from_uint(0);
	bignum_t* two = bignum_from_uint(2);
	bignum_t* result = bignum_from_uint(1);
	bignum_t* t;

	b = bignum_copy(b);
	//printf("%s\n", bignum_to_str(e, 16));
	e = bignum_copy(e);
	//printf("%s\n", bignum_to_str(e, 16));

	while (bignum_cmp(e, zero) > 0) {
		//printf("b %s e %s m %s\n", bignum_to_str(b, 16), bignum_to_str(e, 16), bignum_to_str(m, 16));
		if (e->places[0] % 2 == 1) {
			t = bignum_mul(result, b);
			bignum_delete(result);
			bignum_div(NULL, &result, t, m);
			bignum_delete(t);
		}

		bignum_div(&t, NULL, e, two);
		bignum_delete(e);
		e = t;

		t = bignum_mul(b, b);
		//printf("t %s\n", bignum_to_str(t, 16));
		bignum_delete(b);
		bignum_div(NULL, &b, t, m);
		bignum_delete(t);
	}

	bignum_delete(zero);
	bignum_delete(two);
	bignum_delete(b);
	bignum_delete(e);

	return result;
}

int
bignum_is_prime(bignum_t* n)
{
	uint32_t bases[] = { 2, 3, 5, 7 };
	bignum_t *b, *e, *value, *one;
	int result = 1;

	if (n->places[0] % 2 == 0) {
		return 0;
	}

	one = bignum_from_uint(1);

	for (int i = 0; i < sizeof(bases) / sizeof(bases[0]); i++) {
		b = bignum_from_uint(bases[i]);
		//printf("is prime %s\n", bignum_to_str(n, 16));
		e = bignum_sub(n, one);
		//printf("is prime %s\n", bignum_to_str(e, 16));
		//printf("%s %s\n", bignum_to_str(e, 10), bignum_to_str(n, 10));
		value = bignum_mod_exp(b, e, n);
		//printf("%s %d\n", bignum_to_str(value, 10), value->places[0]);

		bignum_delete(b);
		bignum_delete(e);

		if (bignum_cmp(value, one) != 0) {
			result = 0;
			bignum_delete(value);
			break;
		}

		bignum_delete(value);
	}

	bignum_delete(one);

	return result;
}

bignum_t*
bignum_gcd(bignum_t* a, bignum_t* b)
{
	bignum_t *t, *zero = bignum_from_uint(0);

	a = bignum_copy(a);
	b = bignum_copy(b);

	while (bignum_cmp(b, zero) != 0) {
		bignum_div(NULL, &t, a, b);
		bignum_delete(a);
		a = b;
		b = t;
	}

	bignum_delete(zero);
	bignum_delete(b);

	return a;
}

bignum_t*
bignum_euclidean_extended(bignum_t** pr, bignum_t** ps, bignum_t** pt, bignum_t* a, bignum_t* b)
{
	bignum_t *q, *zero = bignum_from_uint(0);
	bignum_t *t1, *t2;
	bignum_t *r, *old_r;
	bignum_t *s, *old_s;
	bignum_t *t, *old_t;

	old_r = bignum_copy(a);
	r = bignum_copy(b);

	s = bignum_from_uint(0);
	old_s = bignum_from_uint(1);

	t = bignum_from_uint(1);
	old_t = bignum_from_uint(0);

	while (bignum_cmp(r, zero) != 0) {
		bignum_div(&q, &t1, old_r, r);

		old_r = r;
		r = t1;

		t1 = bignum_mul(q, s);
		t2 = bignum_sub(old_s, t1);
		old_s = s;
		s = t2;

		t1 = bignum_mul(q, t);
		t2 = bignum_sub(old_t, t1);
		old_t = t;
		t = t2;
	}

	if (pr) *pr = old_r; else bignum_delete(old_r);
	if (ps) *ps = old_s; else bignum_delete(old_s);
	if (pt) *pt = old_t; else bignum_delete(old_t);

	bignum_delete(zero);

	return a;
}

