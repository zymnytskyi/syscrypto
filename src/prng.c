#include <syscrypto/prng.h>

#include <prng/mersenne_twister.h>

#include <time.h>

prng_t*
prng_new(int generator_type, int seed)
{
	switch (generator_type) {
		case PRNG_GENERATOR_MT:
			return prng_mt_new(seed);
	}

	// some error code
	return (prng_t*)0;
}

prng_t*
prng_new_seeded(int generator_type)
{
	static prng_t* prng = NULL;

	if (!prng) {
		prng = prng_new(PRNG_GENERATOR_DEFAULT, time(NULL));
	}

	return prng_new(generator_type, prng_rand(prng));
}

uint32_t
prng_rand(prng_t* prng)
{
	switch (prng->type) {
		case PRNG_GENERATOR_MT:
			return prng_mt_rand(prng);
	}

	// some error code
	return -1;
}

void
prng_random_bytes(prng_t* prng, uint8_t* bytes, uint32_t num) {
	uint32_t full = num / 4;
	uint32_t* out = (uint32_t*)bytes;
	int i;

	for (i = 0; i < full; i++) {
		out[i] = prng_rand(prng);
	}

	if (num % 4 != 0) {
		uint32_t n = prng_rand(prng);
		uint8_t* nbytes = (uint8_t*)&n;

		for (i = 0; i < num % 4; i++) {
			bytes[full * 4 + i] = nbytes[i];
		}
	}
}

void
prng_delete(prng_t* prng)
{
	switch (prng->type) {
		case PRNG_GENERATOR_MT:
			return prng_mt_delete(prng);
	}
}

